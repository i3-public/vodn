
# os
FROM ubuntu:24.04
ARG DEBIAN_FRONTEND=noninteractive

# apt
RUN apt-get -y update
RUN apt-get -y upgrade
RUN apt-get -y install sudo screen nano iputils-ping wget nload htop net-tools cron libcurl4 ssh

# git
RUN wget -qO- https://gitlab.com/i3-public/conf/-/raw/main/git/README.txt | bash

# ssh
RUN sed -i 's/#PermitRootLogin prohibit-password/PermitRootLogin yes/g' /etc/ssh/sshd_config
RUN service ssh restart

# php
RUN apt-get -y install software-properties-common apt-transport-https
RUN add-apt-repository ppa:ondrej/php -y
RUN apt-get -y update
RUN apt-get -y upgrade
#
RUN apt-get -y install nginx
RUN apt-get -y install php8.3-fpm php8.3-curl php8.3-zip php8.3-gd php8.3-mysql php8.3-xml php8.3-mbstring php8.3-intl
RUN service php8.3-fpm start
#
RUN ln -s /var/run/php/php8.3-fpm.sock /var/run/php.sock
RUN ln -s /usr/sbin/php-fpm8.3 /usr/sbin/php-fpm
RUN ln -s /etc/init.d/php8.3-fpm /etc/init.d/php-fpm
#
RUN rm -rf /etc/nginx/sites-enabled
RUN ln -s /etc/nginx/sites-available /etc/nginx/sites-enabled
#
RUN echo "listen = /var/run/php.sock" >> /etc/php/8.3/fpm/php-fpm.conf
RUN echo "listen.owner = www-data" >>    /etc/php/8.3/fpm/php-fpm.conf
RUN echo "listen.group = www-data" >>    /etc/php/8.3/fpm/php-fpm.conf
RUN echo "listen.mode = 0660" >>         /etc/php/8.3/fpm/php-fpm.conf
#
RUN wget https://gitlab.com/i3-public/conf/-/raw/main/nginx-php/conf/nginx.conf -O /etc/nginx/nginx.conf
RUN wget https://gitlab.com/i3-public/conf/-/raw/main/nginx-php/conf/www.conf   -O /etc/php/8.3/fpm/pool.d/www.conf
RUN wget https://gitlab.com/i3-public/conf/-/raw/main/nginx-php/conf/php8.3.ini -O /etc/php/8.3/fpm/php.ini
#
RUN cd /etc/php/8.3; rm -rf cli/php.ini ; ln -s /etc/php/8.3/fpm/php.ini ./cli/php.ini
#
RUN if [ ! -d /run/php ]; then mkdir /run/php; fi

#
RUN rm -rf /etc/nginx/sites-available/default
RUN ln -s /var/www/conf/default /etc/nginx/sites-available/default

# sudo
RUN echo "www-data ALL=NOPASSWD: ALL" >> /etc/sudoers

# repo
RUN rm -rf /var/www && git clone https://gitlab.com/i3-public/vodn.git /var/www

# start
RUN /etc/init.d/php-fpm restart
RUN service nginx reload

# cron
RUN update-rc.d cron defaults
RUN echo "/usr/sbin/php-fpm" > /etc/rc.local
RUN echo "service ssh start" >> /etc/rc.local
RUN chmod 0777 /etc/rc.local

# cron
RUN wget -qO- https://gitlab.com/i3-public/vodn/-/raw/main/conf/cron.txt > /var/spool/cron/crontabs/root
RUN echo "" >> /var/spool/cron/crontabs/root

# monitoring
RUN wget -qO- https://gitlab.com/i3-public/net-usage/-/raw/master/README.txt | bash -s 8095 skipserver

# end
STOPSIGNAL SIGTERM
CMD /etc/rc.local && /usr/sbin/cron start && /usr/sbin/nginx -g 'daemon off;' && /usr/sbin/php-fpm
