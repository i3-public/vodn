<?php include_once('/var/www/inc/.php');


if( req::get('do') == 'recontent' ){

	if(! $user_id = req::get('user_id') ){
		log::it("no user_id defined");

	} else if(! $stream_id = req::get('stream_id') ){
		log::it("no stream_id defined");

	} else if(! $extn = req::get('extn') ){
		log::it("no extn defined");
	
	} else if(! $hash_old = req::get('hash') ){
		log::it("no hash defined");

	} else {

		$hash_new = hash_generate(10, [ $stream_id, $user_id, $_SERVER['REMOTE_ADDR'] ]);

		if( !defined('DEBUG_MODE') or DEBUG_MODE !== true and $hash_old != $hash_new ){
			log::it("wrong hash: {$hash_old} != {$hash_new}");
		
		} else if(! $v_json = net::wget( SIGNAL_POINT."/api/feed/vodn/get_vod_by_id/?id=$stream_id" ) ){
			log::it("cant get the video path");

		} else if(! $v = json_decode($v_json) ){
			log::it("cant decode the video path from json: {$v_json}");

		} else {

			# 
			# guard
			vod_guard($user_id, $stream_id);
			#

			#
			# do the job
			recontent($v);
			#

		}

		echo 'contenuti multimediali non accessibili.';

	}
		
} else {
	echo "va avanti e avanti e avanti.";
}



