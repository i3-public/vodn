<?php include_once('/var/www/inc/.php');



# 
# semaphpre
proc::semaphore(['sync_conn.php'], ['sudo']);
#


# 
# if its run from cron, skip it
if( is_array($argv) and array_key_exists(1, $argv) ){


	# 
	# take care of log::dot
	global $flag_log_dot_i;
	if( is_array($argv) and array_key_exists(2, $argv) ){
		$flag_log_dot_i = $argv[2];
	} else {
		shell_exec(" echo \"\n\" >> /tmp/dot ");
	}
	#


	# 
	# netstat init
	netstatMonitor::init();
	#


	# 
	# sync the local database with db
	port_conn_sync();
	# 


	#
	# start the sync
	netstatMonitor::sync( 
		
		8095, 
		30, 
		'port_conn_add/port_conn_remove'

	);


}


# 
# proc loop
log::dot(' ');
$pwdcode = ( (is_array($argv) and array_key_exists(1,$argv)) ? $argv[1] : proc::pwdcode() );
shell_exec(" /usr/bin/php /var/www/cron/sync_conn.php {$pwdcode} {$flag_log_dot_i} >> /tmp/sync_conn 2>&1 & ");
die;
#




function port_conn_add( $item_s ){

    if( sizeof($item_s) ){

        $server_id = 9001;
	    $date_start = date('U');

		$kill_s = [];
		$values = [];

		foreach( $item_s as $ip_port => $info ){

		    list($pid, $user_agent, $time, $fingerprint_params) = $info;
		    list($user_id, $stream_id, $container) = $fingerprint_params;

			list($status, $data) = port( '/conx/cur_n_max/', 'POST', [
				'user_id' => $user_id,
				'ignore' => $stream_id
			], false);
			if( $status != 'OK' ){
				log::it("status: $status, code: $data");
				return false;
			}

			extract($data);

			log::it("sync: cur $cur, max $max");

			if( $cur >= $max ){
				$kill_s[] = $pid;
			
			} else {
			
				$user_agent = base64_decode($user_agent);
				$user_ip = explode('_', $ip_port)[0];
				$date_start = round($time / 1000);

				$values[] = [
					$user_id,
					$stream_id,
					$server_id,
					$user_agent,
					$user_ip,
					$container,
					$pid,
					$date_start,
					$ip_port,
				];
			
			}

		}

		if( sizeof($kill_s) ){
			$kill_str = implode(' ', $kill_s);
			shell_exec(" sudo kill {$kill_str} ");
			log::it("kill ".$kill_str." by sync_conn");
		}

		if( sizeof($values) ){

			list($status, $code) = port( '/conx/insert/', 'POST', [
				'column' => ' user_id stream_id server_id user_agent user_ip container pid date_start ip_port ',
				'values' => $values,
			], false);
			if( $status != 'OK' )
				return false;

		    echo "conx insert res: {$code}\n";

		}
		
		return true;

	}

}


function port_conn_remove( $ip_port_s ){

    $values = [];

	if( sizeof($ip_port_s) ){

	    $server_id = 9001;

	    foreach( $ip_port_s as $ip_port ){
	        $values[] = [
		    	$server_id,
		    	$ip_port,
		    ];
		}

		if( sizeof($values) ){

		    list($status, $code) = port( '/conx/delete/', 'POST', [
		    	'column' => ' server_id ip_port ',
		    	'values' => $values,
		    ], false);

		    echo "conx delete res: {$code}\n";
	    	return $status == 'OK';

		}

		return false;

	}

}


function port_conn_sync(){
	
	$server_id = 9001;

	// do a list port, to get the current list
	list($status, $db_s) = port( '/conx/find/', 'POST', [
		'need' => ' ip_port ',
		'filter' => [
			'server_id' => $server_id
		]
	], false);
	foreach( $db_s as $i => $db ){
		$db_s[ $i ] = $db['ip_port'];
	}

	// list of records on sync.json
	$sync_s = file_get_contents('/tmp/netstatMonitor/sync.json');
	$sync_s = json_decode($sync_s, true);

	// az local, harchi tu db nis, add beshe
	$add_item_s = [];
	foreach( $sync_s as $ip_port => $info ){
		if(! in_array($ip_port, $db_s) ){
			// add
			$add_item_s[ $ip_port ] = $info;
		}
	}

	// az db, harchi tu local nis, remove beshe
	$remove_item_s = [];
	foreach( $db_s as $ip_port ){
		if(! in_array($ip_port, array_keys($sync_s)) ){
			// remove
			$remove_item_s[] = $ip_port;
		}
	}

	echo json_encode(array_keys($add_item_s)).', '.json_encode($remove_item_s). " synced\n";

	// do add port
	if( sizeof($add_item_s) ){
		if( port_conn_add($add_item_s) ){
			$add_item_s = [];
		}
	}

	// do remove port
	if( sizeof($remove_item_s) ){
		if( port_conn_remove($remove_item_s) ){
			$remove_item_s = [];
		}
	}

}
