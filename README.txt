
# main port: 8095
# wget -qO- https://gitlab.com/i3-public/vodn/-/raw/main/README.txt | bash


#
# docker installer
wget -qO- https://gitlab.com/i3-public/conf/-/raw/main/docker/README.txt | bash

#
# remove old docker
docker rm -f vodn
docker rmi vodn-image

# 
# build image
docker build -t vodn-image https://gitlab.com/i3-public/vodn/-/raw/main/conf/dockerfile

#
# run docker
docker run -t -d --restart unless-stopped --name vodn --hostname vodn -p 8094:22 -p 8095:8095 vodn-image

