<?php


function vod_guard( $user_id, $stream_id ){

    #
    # get user live conn, check if user is over
    list($status, $data) = port( '/conx/cur_n_max/', 'POST', [ 
        'user_id' => $user_id,
        'ignore' => $stream_id
    ], false);
    if( $status != 'OK' ){
        log::it("status: $status, code: $data");
        return false;
    }

    extract($data);

    log::it("guard: cur $cur, max $max");

    # 
    # no more connection, kill with weed
    if( $max != 0 and $cur >= $max ){

        $file = 'http://lib.i3ns.net/media/video/max_connection.mp4';

        header('Content-type: video/mp4');
        header("Accept-Ranges: 0-118008");

        echo file_get_contents($file);

        log::it("kill by guard");
        die;

    #
    # welcome
    } else {
        return true;
    }
    

}


