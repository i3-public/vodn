<?php


# 2024/11/20
class arr {

    # 2024/11/20
    public static function is_associative( array $arr ){
        if (array() === $arr) return false;
        return array_keys($arr) !== range(0, count($arr) - 1);
    }
    
}


class net {

    # 2024/11/19
    public static function wget( $url, $etc=[ /* post, timeout, user_agent, display_error */ ] ){

        if( array_key_exists('timeout', $etc) ){
            $http_arr['timeout'] = intval($etc['timeout']);
        }

        $user_agent = array_key_exists('user_agent', $etc) 
            ? $etc['user_agent'] 
            : 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Safari/537.36';
            
        $http_arr['header'] = "User-Agent: {$user_agent}\r\n";

        if( array_key_exists('post', $etc) and sizeof($etc['post']) ){
            $http_arr['header'].= 'Content-Type: application/x-www-form-urlencoded';
            $http_arr['method'] = 'POST';
            $http_arr['content'] = http_build_query($etc['post']);
        }

        $contx = stream_context_create([
            'http' => $http_arr,
            'ssl' => [ 'verify_peer'=>false, 'verify_peer_name'=>false ],
        ]);

        try {
            $res = file_get_contents($url, false, $contx);

        } catch (Exception $e) {
            if( array_key_exists('display_error', $etc) and $etc['display_error'] ){
                echo $e->getMessage();

            } else {
                proc::error($code);
            }
        }

        return $res;
        
    }

    # 2024/11/19
    public static function wjson( $url, $etc=[ /* post, timeout, user_agent, display_error */ ], $die=null ){
        
        if(! $res = self::wget( $url, $post_params, $etc ) ){
            $code = "no content received";

        } else if(! json::is($res) ){
            $code = "the result is not in the format of json";

        } else if(! $res = json_decode(trim($res), true) ){
            $code = "can't decode the json";
            
        } else if( !array_key_exists('status', $res) or !array_key_exists('code', $res) ){
            $code = "parameters not properly defined in result";

        } else if( $res['status'] != 'OK' ){
            $code = $res['code'];

        } else {
            return $res['data'];
        }

        if( $die ){
            die($code);

        } else {
            return proc::error($code);
        }

    }

    # 2024/11/20
    public static function host_ip(){

        $ip = trim(shell_exec('hostname -I'), "\r\n\t ");

        if( strstr($ip, ' ') ){        
            $ip = explode(' ', $ip)[0]; 
        }
        
        return $ip;

    }

    # 2024/11/20
    public static function is_ip( $str ){
        $ret = filter_var($str, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4);
        return $ret;
    }


}


class proc {

    # 2024/11/19
    private static array $error_arr;
    
    # 2024/11/19
    public static function error( $message='--no-default-value--' ){
        
        # return back, and clear the array
        if( $message == '--no-default-value--' ){
            $error_arr = self::$error_arr;
            self::$error_arr = [];
            return $error_arr;

        } else {
            self::$error_arr[] = $message;
            return false;
        }
        

    }

    # 2024/11/23
    public static function semaphore( $include_proc, $exclude_proc=[], $wait=null, $die_message=null ){
        
        $include_proc[] = self::pwdcode();

        while( true ){
            if( sizeof( self::curr_proc($include_proc, $exclude_proc) ) > 1 ){
                if( $wait !== null ){
                    time::sleep($wait);

                } else {
                    echo ($die_message ? str_replace('%', 'locked by semaphore', $die_message) : 'locked by semaphore') ."\n";
                    die;
                }

            } else {
                break;
            }
        }

    }

    # 2024/11/20
    public static function curr_proc( $incl=null, $excl=null, $skip_sh_c=false, $command='ps aux' ){
    
        $curr_cmd = $command.' | grep -v grep';
    
        if(! $skip_sh_c ){
            $curr_cmd.= " | grep -v 'sh -c'";
        }
    
        if( $incl ){
            if(! is_array($incl) ){
                $curr_cmd.= " | grep '$incl'";
    
            } else foreach( $incl as $word ){
                $curr_cmd.= " | grep '$word'";
            }
        }
    
        if( $excl ){
            if(! is_array($excl) ){
                $curr_cmd.= " | grep -v '$excl'";
    
            } else foreach( $excl as $word ){
                $curr_cmd.= " | grep -v '$word'";
            }
        }
    
        // echo $curr_cmd;die();
        $ps_s = shell_exec($curr_cmd);
        $curr = [];
    
        foreach( explode("\n", $ps_s) as $ps ){
            if( $ps = trim($ps, "\r\n\t ") ){
                $curr[] = $ps;
            }
        }
    
        return $curr;
    
    }
    
    # 2024/11/24
    public static function pwdcode(){
        
        $code = shell_exec(" cat /etc/machine-id ");
        $code = $code ? trim($code, "\r\n\t ") : '';
        $code = md5($code);
        
        return $code;
        
    }
    
}


class code {

    # 2024/11/20
    public static function array_from_json( $json ){
    
        if( $json ){
            if( $json != 'null' ){
                $json = json_decode($json, true);
                if( sizeof($json) ){
                    return $json;
                }
            }
        }
    
        return [];
        
    }

    # 2024/11/20
    public static function start_with( $text, $start ){
        return ( substr($text, 0, strlen($start) ) == $start );
    }

    # 2024/11/20
    public static function between( $text, $start, $end ){

        $text = explode($start, $text)[1];
        $text = explode($end, $text)[0];

        return $text;

    }

    # 2024/11/20
    public static function force_zero( $code, $zero_count ){

        $need = $zero_count - strlen($code);

        if( $need > 0 ){
            $code = str_repeat('0', $need) . $code;
        }

        return $code;

    }


}


class json {

    # 2024/11/20
    public static function die( array $arr ){
        header('Content-Type: application/json; charset=utf-8');
        echo json_encode($arr);
        die;
    }
    
    # 2024/11/20
    public static function is( $text ){
        json_decode($text);
        $res = json_last_error() === JSON_ERROR_NONE;
        return $res;
    }

}


class time {

    # 2024/11/20
    public static function sleep( $sec ){
        $sec = round( $sec * 1000000 );
        usleep($sec);
    }

    # 2024/11/20
    public static function milli(){
        $milliseconds = round( microtime(true) * 1000);
        return $milliseconds;
    }

}


class log {

    # 2024/11/20
    public static function it( $code, $path='/tmp/log', $breakLine=true ){

        if( defined('DEBUG_MODE') and DEBUG_MODE === true ){

            if( is_object($code) ){
                $code = (array)$code;
            }

            if( is_array($code) ){
                $code = json_encode($code);
            }

            if( $breakLine ){
                $code.= "\n";
            }

            if( $fp = fopen($path, 'a+') ){
                fwrite($fp, $code);
                fclose($fp);
            }
        
        }

        return $code;

    }

    # 2024/11/20
    public static function dot( $code='.' ){

        if( strlen($code) > 1 ){
            for( $i=0; $i<strlen($code); $i++ ){
                self::dot( substr($code, $i, 1) );
            }
            return;
        }

        global $flag_log_dot_i;
        if( $flag_log_dot_i > 40 )
            $flag_log_dot_i%= 40;

        self::it($code, '/tmp/dot', !(++$flag_log_dot_i%40) );

    }

    
}


class file {

    # 2024/11/20
    public static function safe_put( $file, $data ){

        file_put_contents( $file."_tmp", $data );
        
        if( file_exists($file) ){
            rename( $file, $file."_remove" );
            rename( $file."_tmp", $file );
            unlink( $file."_remove" );

        } else {
            rename( $file."_tmp", $file );      
        }

    }

}



# 2024/11/25
class req {

    # 2024/11/25
    public static function get( $key ){

        if( !array_key_exists($key, $_GET) or !$value = $_GET[ $key ] ){
            return null;

        } else {
            return $value;
        }
        
    }

    # 2024/11/25
    public static function post( $key ){

        if( !array_key_exists($key, $_POST) or !$value = $_POST[ $key ] ){
            return null;

        } else {
            return $value;
        }
        
    }

    # 2024/11/25
    public static function argv( $i ){

        if( !isset($argv) or !is_array($argv) or !array_key_exists($i, $argv) or !$value = $argv[ $i ] ){
            return null;

        } else {
            return $value;
        }
        
    }

}