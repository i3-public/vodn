<?php


# 2024/11/19
function port( $path, $method='GET', $params=[], $die_on_error=false ){

    $url = PORT_NODE . $path;

    # POST
    if( strtolower($method) == 'post' ){

        $code = file_get_contents($url, false, stream_context_create([
            'http' => [
                // 'timeout' => $timeout,
                'method'  => 'POST',
                'header'  => 'Content-Type: application/x-www-form-urlencoded',
                'content' => http_build_query($params)
            ]
        ]));

    # GET
    } else {
        $code = file_get_contents( $url . ( sizeof($params) ? '/?'.http_build_query($params) : '' ) );
    }

    if(! $code ){

        if( $die_on_error ){
            echo is_bool($die_on_error)
                ? "can't port the result. {$code}\n"
                : str_replace('%', $code, $die_on_error)."\n";
            die;

        } else {
            return [ false, "can't port the result. {$code}\n" ];
        }

    } else {

        $json = json_decode($code, true);
        
        if(! is_array($json) ){
            echo "PORT returned wrong content: \n-----------------------\n{$code}\n-----------------------\n";
            die;

        } else if( array_key_exists('status', $json) and $json['status'] != "OK" ){
            
            if( $die_on_error ){
                
                echo is_bool($die_on_error)
                    ? "can't port the result. {$code}\n"
                    : str_replace('%', $code, $die_on_error)."\n";

                die;
    
            } else {
                return [ false, "can't port the result. {$code}\n" ];
            }

        } else {
            
            $data = array_key_exists('code', $json)
                ? $json['code']
                : ''
                ;

            return $die_on_error
                ? $data
                : [ true, $data ]
                ;

        }

    }

}


