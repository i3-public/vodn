<?php


# 2024/11/25
class netstatMonitor {


    private static string $TempDIR = '/tmp/' . __CLASS__ . '/';
    private static string $SyncFile = '/tmp/' . __CLASS__ . '/sync.json';


    public static function init(){
        if(! file_exists(self::$SyncFile) ){
            if(! file_exists(self::$TempDIR) ){
                shell_exec(" sudo mkdir " . self::$TempDIR );
                shell_exec(" sudo chown -R www-data:www-data " . self::$TempDIR );
            }
            shell_exec(" sudo echo '[]' > " . self::$SyncFile );
        }
    }
    

    // add .conn to /tmp
    public static function add( $fingerprint_params = [] ){

        $fingerprint_path = self::$TempDIR . $_SERVER['REMOTE_ADDR'] . '_' . $_SERVER['REMOTE_PORT'] . '.conn';

        if(! file_exists($fingerprint_path) ){
            
            $fingerprint_data = json_encode([ 
                getmypid(),
                base64_encode($_SERVER['HTTP_USER_AGENT']),
                time::milli(),
                $fingerprint_params
            ]);
            
            file_put_contents($fingerprint_path, $fingerprint_data);

        }
        
    }


    // add .disc to /tmp
    public static function remove(){

        $fingerprint_path = self::$TempDIR . $_SERVER['REMOTE_ADDR'] . '_' . $_SERVER['REMOTE_PORT'] . '.disc';

        if(! file_exists($fingerprint_path) ){            
            file_put_contents($fingerprint_path, 'nil');
        }
        
    }


    // counter_of_death, it can be a number, for example 10, for 10 loops,
    //   it will stop after 10, and return back
    // reaction_func_string, it can be soemthig like this 'conn_add/conn_remove'
    public static function sync( $service_port, $counter_of_death=null, $reaction_func_string=null ){
        
        $counter_of_death_i = 0;
        $items_for_funcAdd = [];
        $items_for_funcRemove = [];

        while (true) {

            # list of local database
            $sync_s = file_get_contents(self::$SyncFile);
            $sync_s = json_decode($sync_s, true); // [ ..., [ ip_port => [ pid, user_agent, time, [ "user_id", "stream_id" ] ] ], ... ]

            # list of current netstat process
            $curr_s = self::curr($service_port); // [ ..., "212.24.111.152_57819", "85.215.179.48_40406", ... ]
            
            # list of conn [ ip_port => [ pid, user_agent, time, [ "user_id", "stream_id" ] ] ]
            $conn_s = [];
            foreach( glob( self::$TempDIR . '*.conn') as $conn_path ){
                $ip_port = substr(strrchr($conn_path, '/'), 1, -5);
                $conn_s[ $ip_port ] = json_decode( file_get_contents($conn_path), true );
                // unlink($conn_path);
            }
            $disc_s = [];
            # list of disc [ ..., ip_port, ... ]
            foreach( glob( self::$TempDIR . '*.disc') as $disc_path ){
                $ip_port = substr(strrchr($disc_path, '/'), 1, -5);
                $disc_s[] = $ip_port;
                unlink($disc_path);
            }

            # 
            # curr_s loop
            foreach( $curr_s as $ip_port ){

                # reported to add, add it to sync
                if(! array_key_exists($ip_port, $sync_s) ){

                    # ADD
                    if( array_key_exists($ip_port, $conn_s) ){
                        
                        $sync_s[ $ip_port ] = $conn_s[ $ip_port ]; // add
                        $items_for_funcAdd[ $ip_port ] = $sync_s[ $ip_port ]; // add to add stack
                        
                        // remove the related conn
                        unlink( self::$TempDIR .$ip_port. '.conn');
                        unset($conn_s[ $ip_port ]);

                        log::dot('A');

                    # SKIP
                    } else {
                        log::dot('S');
                    }
                }

                # reported to kill + remove from database
                if( in_array($ip_port, $disc_s) ){
                    
                    // remove the related disc
                    // unlink( self::$TempDIR .$ip_port. '.disc');

                    $pid = $sync_s[ $ip_port ][0];
                    shell_exec(" sudo kill {$pid} ");
                    log::it("kill ".$pid." by netstatMonitor");
                    log::dot('K');

                    unset($sync_s[ $ip_port ]); // remove
                    $items_for_funcRemove[] = $ip_port; // add to remove stack
                    log::dot('R');

                }

            }
            # 
            

            # 
            # sync_s loop
            # 
            # sync the curr_s firast
            $curr_s = self::curr($service_port);
            # 
            foreach( $sync_s as $ip_port => $info ){

                if(! in_array($ip_port, $curr_s) ){
                    
                    // its stoppped manually
                    // remove from sync

                    unset($sync_s[ $ip_port ]); // remove
                    $items_for_funcRemove[] = $ip_port; // add to remove stack
                    log::dot('R');

                }
            }
            #
            

            # 
            # conn_s loop
            foreach( $conn_s as $ip_port => $info ){
                if(! in_array($ip_port, $curr_s) ){
                    
                    // process stoppped, but the .conn is not checked
                    // remove the .conn
                    
                    unlink( self::$TempDIR .$ip_port. '.conn');
                    unset($conn_s[ $ip_port ]);
                    
                    log::dot('I');

                }
            }
            #

            
            #
            # store the sync arr in json
            file_put_contents( self::$SyncFile, ($sync_s == null) ? '[]' : json_encode($sync_s, JSON_PRETTY_PRINT) );
            #


            #
            # func
            # here we try to send all new conn/disc to database, through the port
            // log::it('items_for_func '.json_encode(array_keys($items_for_funcAdd)) .', '.json_encode($items_for_funcRemove));

            if( ( sizeof($items_for_funcAdd) or sizeof($items_for_funcRemove) ) and $reaction_func_string ){
                
                list($funcAdd, $funcRemove) = explode('/', $reaction_func_string);

                # funcAdd
                if( function_exists($funcAdd) and sizeof($items_for_funcAdd) ){
                    if(! $funcAdd( $items_for_funcAdd ) ){
                        log::dot('fA');
                    } else {
                        $items_for_funcAdd = [];
                    }
                }

                # funcRemove
                if( function_exists($funcRemove) and sizeof($items_for_funcRemove) ){
                    if(! $funcRemove( $items_for_funcRemove ) ){
                        log::dot('fR');
                    } else {
                        $items_for_funcRemove = [];
                    }
                }

            }

            # 


            # 
            # death, or sleep
            if( $counter_of_death ){
                if( ++$counter_of_death_i >= $counter_of_death ){
                    break;
                }
            }
            #
            log::dot();
            time::sleep(1);
            #


        }

        /*
        * load the $arr from $db
        * check the /tmp directory, add the new ones, remove the old one to the $arr
        * sync the $arr with $db
        * 
        */

    }


    public static function curr( $service_port ){
    
        $sv_ip_port = net::host_ip() . ':' . $service_port;
    
        $cmd = " sudo netstat -ano -p ";
        $cmd.= " | grep ESTABLISHED ";
        // $cmd.= " | grep -v ' 0 {$sv_ip_port} ' ";
        $cmd.= " | grep ' {$sv_ip_port} ' ";
        $cmd.= " | awk {'print \$5'} ";
    
        if(! $sh = shell_exec($cmd) )
            return [];

        $sh = trim($sh, "\r\n\t ");
        $sh = str_replace(["\r\n","\n","\t"], " ", $sh);
        $sh = str_replace(":", "_", $sh);
        $sh = trim($sh, "\r\n\t ");
        
        return $sh 
            ? explode(" ", $sh)
            : []
            ;
        
    }
    

}

