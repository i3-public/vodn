<?php


function recontent( $v ){

    $name = substr(strrchr($v, "/"), 1);
    $extn = substr(strrchr($v, "."), 1);

    $the_head = get_headers( $v, TRUE );
    if( strstr( $the_head[0], '302 Found' ) and $the_head['Location'] ){
        $v = $the_head['Location'];
        $the_head = get_headers( $v, TRUE );
    }

    foreach( $the_head as $k => $j ){
        if( $k == "0" ){
            header('HTTP/1.1 206 Partial Content');
            continue;
        } else if( $k == 'Accept-Ranges' ){
            continue;
        } else if( is_array($j) ){
            $j = implode(', ', $j);
        }
        header( $k . ': ' . $j );
    }

    header("Content-Type: video/".$extn );
    header('Content-disposition: filename="'.$name.'"');

    $head = array_change_key_case( $the_head );
    $filesize = $head['content-length'];

    $offset = 0;
    $length = $filesize;
    if (isset($_SERVER['HTTP_RANGE'])) {
        preg_match('/bytes=(\d+)-(\d+)?/', $_SERVER['HTTP_RANGE'], $matches);
        $offset = intval($matches[1]);
        $length = $filesize - $offset - 1;
       $partialContent = "true";
    } else {
       $partialContent = "false";
    }

    if ($partialContent == "true") {
        header('HTTP/1.1 206 Partial Content');
        header('Accept-Ranges: bytes');
        header('Content-Range: bytes '.$offset.
            '-'.($offset + $length).
            '/'.$filesize);
    } else {
        header('Accept-Ranges: bytes');
    }

    $ch = curl_init();

    if (isset($_SERVER['HTTP_RANGE'])) {
        $partialContent = true;
        preg_match('/bytes=(\d+)-(\d+)?/', $_SERVER['HTTP_RANGE'], $matches);
        $offset = intval($matches[1]);
        $length = $filesize - $offset - 1;
        $headers = array(
            'Content-disposition: filename="Bruce Lee.'.$extn.'"',
            'Range: bytes='.$offset.'-'.($offset + $length)
        );
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    }


    #
    # add to user_activity_now
    global $user_id, $stream_id;
    netstatMonitor::add([ $user_id, $stream_id, $extn ]);
    # 

    
    # 
    # curl it
    curl_setopt($ch, CURLOPT_VERBOSE, 1);
    curl_setopt($ch, CURLOPT_TIMEOUT, 222222);
    curl_setopt($ch, CURLOPT_URL, $v);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.96 Safari/537.36");
    curl_setopt($ch, CURLOPT_NOBODY, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, false);
    // curl_setopt($ch, CURLOPT_BUFFERSIZE, 1 * 1024*1024 );
    curl_setopt($ch, CURLOPT_FORBID_REUSE, true);

    curl_exec($ch);

}


